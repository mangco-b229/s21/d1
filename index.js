

// An array in programming is simply a list of data

// let studentNumber1 ="2020 - 1923"
// let studentNumber1 ="2020 - 1924"
// let studentNumber1 ="2020 - 1925"
// let studentNumber1 ="2020 - 1926"
// let studentNumber1 ="2020 - 1927"

// Now, with array, we can simply write the data in a single varible like this

let studentNumbers = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"]

// [SECTION] - Array
let grades = [98, 94, 89, 90]
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitso"]

// Possible use of array but not recommended 
let mixedArr = [12, "Asus", null, undefined, {}]

console.log(grades)
console.log(computerBrands)
console.log(mixedArr)

let myTasks = [
	"drink html",
	"eat javascript",
	"Eat bootstrap"
	]

let city1 = "tokyo"
let city2 = "Manila"
let city3 = "Jakarta"

let cities = [city1, city2, city3]
console.log(myTasks.length)
console.log(cities.length)

// [SECTION] - length Property
// The .length property allows us to get and set the total number of items in an array

let blankArr = []

console.log(blankArr.length)

// length property can also be used with strings. Some array methods and properties cn also be used with strings.

let fullName = "Jamie Noble"
console.log(fullName.length)

// length property can also set the total number of items in an array

myTasks.length -= 1
console.log(myTasks)


cities.length++
console.log(cities)

//[SECTION] - Readings from array
console.log(grades[0])
console.log(computerBrands[3])

// accessing an array element that does not exist will return "undefined"
console.log(grades[20])

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]
// Access the second item in the array
console.log(lakersLegends[1])

// you can alose save/store array items in  another variable
let currentLakers = lakersLegends[2]

console.log(currentLakers)

lakersLegends[2] = "Gasol"
console.log("Array after re-assignment")
console.log(lakersLegends)


// acessing the last element of an array
// since the first elemtent start at 0,subtracting 1 to the length of an arary will offset the value by one allowing us to get the last element


let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]

let lastElementIndex = bullsLegends.length - 1

console.log(bullsLegends[lastElementIndex])

// adding item into an aray
// using indeces, you can al add items in to the array

let newArray = []

console.log(newArray[0])

newArray[0] = "Cloud Strife"

console.log(newArray)

newArray[1] = "Tiffany Lockhar"

console.log(newArray)

newArray[newArray.length] = "Barret Wallace"

console.log(newArray)

//looping over an array

let numbers = [5, 12, 30, 46, 40]

for(let i = 0; i < numbers.length; i++) {
	if(numbers[i] % 5 === 0) {
		console.log(numbers[i] + " is divisible by 5")
	} else {
		console.log(numbers[i] + " is not divisible by 5")
	}
}

// [SECTION]

let chessboard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
]

console.table(chessboard)
console.log(chessboard[1][4])

console.log("Pawn moves to:" + chessboard[1][5])